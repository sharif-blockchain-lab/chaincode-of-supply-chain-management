package main

const (

	//1EJ to chainCode ej ratio
	EjBaseRatio = 1000000 // 10^6

	//EJ Bank Start Amount (must multiply with base ratio)
	BankStartDeposit = 110000000 // 110,000,000

	TotalQueryLimit = 100000

	//EJ Bank Key
	EJBankKey = "Bank"

	//Composite key structs
	FeeDeltaIndexName      = "FeeDelta~feeAmountStr~txid~sender"
	TransferDeltaIndexName = "receiverAccountNumber~amountStr~txID"

	// Define Status codes for the response
	OK    = 200
	ERROR = 500

	//fees
	//MinimumOrdinaryFee uint64  = 500
	//MinimumBusinessFee uint64 = 100

	//keys
	//FeeHolderKey = "feeHolder"
	BaseTimeKey = "baseTime"

	//intervals
	OneMonthInterval = 1.0//43200.0

	// Invokable function names
	TRANSFER                     = "transfer"
	GetBalance                   = "getBalance"
	GetTransactionHistoryForUser = "getTransactionHistoryForUser"
	GetTransactionHistory 		 = "getTransactionHistory"
	TEST                         = "test"
	AddUser						 = "addUser"
    DeleteUser					 = "deleteUser"
    AddAsset					 = "addAsset"
    ChangeHolder				 = "changeHolder"
	IssueForUser                 = "issueForUser"
	ChangeProperties             = "changeProperties"
	ChangeStatus				 = "changeStatus"
	GetAllAssetHistory			 = "getAllAssetHistory"
	GetOneAssetHistory           = "getOneAssetHistory"

	//Object types
	TransactionObjectType = "transaction"
	UserObjectType        = "user"
	AssetObjectType		  = "asset"

	// Account Type  (all of these are called USER and are created according to the USER struct)

	SOURCE    = "source"         // people who catch fish, people in the process of supply chain
	SUPPLIER  = "supplier"		 // people who are middle supply chain management
	ENDUSER   = "enduser"		 // people who are end users (Do not read it endurser! It is end user. )
	AdminOrg1 = "adminOrg1"
	AdminOrg2 = "adminOrg2"

	// Asset Type
	Caviar	  = "caviar"

	//ORG Account numbers:

	//EJ CLUB:
	EjClubAccountNumber = "5"

	//EJ SUPPLY AND SUPERVISION CENTER
	EjSupervisorAccountNumber           = "1"
	EjSupervisorExpiredEjsAccountNumber = "2"
	EjSupervisorPublicAccountNumber     = "3"
	EjSupervisorFeesAccountNumber       = "4"

)
