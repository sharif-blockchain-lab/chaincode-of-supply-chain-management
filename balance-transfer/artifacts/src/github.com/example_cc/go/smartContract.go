
package main
import (

	//"bytes"
	//"crypto/x509"
	"encoding/json"
	//"encoding/pem"
	//"errors"
	"fmt"
	//"github.com/golang/protobuf/ptypes"
	//"github.com/golang/protobuf/ptypes/timestamp"
	//"strconv"
	"time"

	//"github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

type SmartContract struct{}


// Init is called when smart contract is instantiated
// The smart contract receives four main arguments in the beginning to initiate the contract
// All the admins of 2 organizations are added in this part
// arg[0] = AccountNumber of admin 1
// arg[1] = AccountNumber of admin 2
// arg[2] = Pubkey of admin 1
// arg[3] = Pubkey of admin 2

func  (s *SmartContract) Init (APIstub shim.ChaincodeStubInterface) peer.Response {
	// Get the args from the transaction proposal
	_, args := APIstub.GetFunctionAndParameters()
	if len(args) != 4{
		return shim.Error("Incorrect arguments. Expecting 4 inputs!")
	}
	// set up any variables or assets here by calling stub.PutState()
	// store the key and the value on the ledger

	// assigning input values to related values of each admin

	admin1_AccountNumber := args[0]
	admin2_AccountNumber := args[1]
	admin1_PublicKey := args[2]
	admin2_PublicKey := args[3]

	// forming the struct corresponding to admin1
	Admin_1 := &User{
		ObjectType: UserObjectType,
		Balance:0,
		Type:AdminOrg1,
		PublicKey:admin1_PublicKey,
		AccountNumber:admin1_AccountNumber,
	}
	// forming the struct corresponding to admin2
	Admin_2 := &User{
		ObjectType: UserObjectType,
		Balance:0,
		Type:AdminOrg2,
		PublicKey:admin2_PublicKey,
		AccountNumber:admin2_AccountNumber,
	}
  /// hello
	// transform the structs to a json object in order to be able to save it on the ledger
	admin1_JsonAsBytes, err := json.Marshal(Admin_1)
	if err!= nil {
		return shim.Error(err.Error())
	}

	admin2_JsonAsBytes, err := json.Marshal(Admin_2)
	if err!= nil {
		return shim.Error(err.Error())
	}

	// storing the information of Admins on the ledger and notifying if an error occurred
	err = APIstub.PutState(admin1_AccountNumber, admin1_JsonAsBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to put state of Admin of Org1: %s", err.Error()))
	}

	err = APIstub.PutState(admin2_AccountNumber, admin2_JsonAsBytes)
	if err != nil {
		return shim.Error(fmt.Sprintf("Failed to put state of Admin of Org2: %s", err.Error()))
	}
	// saving chaincode creation timestamp
	baseTime := time.Now()

	baseTimeAsBytes, err := json.Marshal(baseTime)
	if err!= nil {
		return shim.Error(err.Error())
	}

	putErr := APIstub.PutState(BaseTimeKey, baseTimeAsBytes)
	if putErr != nil {
		return shim.Error(fmt.Sprintf("Failed to put state: %s", putErr.Error()))
	}


	var StartDeposit uint64 = uint64(BankStartDeposit * EjBaseRatio)
	putErr = APIstub.PutState(EJBankKey, []byte(fmt.Sprint(StartDeposit)))

	if putErr != nil {
		return shim.Error(fmt.Sprintf("Failed to put state: %s", putErr.Error()))
	}

	return shim.Success(nil)
}

// Invoke routes invocations to the appropriate function in chaincode
// Current supported invocations are:
//	- addUser      (admin only)
//	- deleteUser   (admin only)
//	- transfer
//	- changeHolder (user only)
//	- addAsset     (user only)	     can't admins create assets?
//	- setBalance   (admin only)
//	- getBalance   (admin and user)  do micro have a balance too?

func (s *SmartContract) Invoke (APIstub shim.ChaincodeStubInterface) peer.Response {
	// validate the sender at the begining
    // validate if the sender is an asset
	// check if requested account number matches public key

	requestSender, err1 := validateSender(APIstub)
	requestAsset, err2  := validateAsset(APIstub)

	if err1 != nil && err2 !=nil {
		return shim.Error("Invalid Asset or Invalid User!")
	}

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// connect each function with the appropriate handler

	if err1 == nil {   // a user is trying to call a function
		switch function {
		case TRANSFER:    //??
			return s.transfer(APIstub, requestSender, args)
		case GetBalance:  // ??
			return s.getBalance(APIstub, requestSender, args)
		case GetTransactionHistoryForUser:   //??
			return s.getTransactionHistoryForUser(APIstub, requestSender, args)
		case GetTransactionHistory:   //??
			return s.getTransactionHistory(APIstub, requestSender, args)
		case GetAllAssetHistory:   //??
			return s.getAllAssetHistory(APIstub, requestSender, args)
		case GetOneAssetHistory:   //??
			return s.getOneAssetHistory(APIstub, requestSender, args)
		case AddUser:
			return s.addUser(APIstub, requestSender, args)
		case DeleteUser:             //???
			return s.deleteUser(APIstub, requestSender, args)
		case AddAsset:
			return s.addAsset(APIstub, requestSender, args)
		case ChangeHolder:
			return s.changeHolder(APIstub, requestSender, args)
		case ChangeStatus:
			return s.changeStatus(APIstub, requestSender, args)
		case IssueForUser:            //?
			return s.issueForUser(APIstub, requestSender, args)

		// for testing certificate attributes
		case TEST:
			return s.test(APIstub, args)

		default:
			return shim.Error("Invalid Smart Contract function name.")
		}
	}
	if err2 == nil {
		switch function {
			case ChangeProperties:
				return s.changeProperties(APIstub, requestAsset , args)
			default:
				return shim.Error("Invalid Smart Contract function name.")
		}
	}
	return shim.Error("Invalid Operation or Input.")
}
